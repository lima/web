# Lima

This page now lives in Mesa upstream:
[https://docs.mesa3d.org/drivers/lima.html](https://docs.mesa3d.org/drivers/lima.html)

[This](https://gitlab.freedesktop.org/lima/web/-/blob/519a5b26596cfe46c3e4dc80d802780b2b3c61b1/README.md) was the last version of the page in this repository before it was moved.
